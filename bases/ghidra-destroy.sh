#!/usr/bin/env bash

kubectl delete deployments.apps ghidra-server-${1}
kubectl delete svc ghidra-service-${1}
kubectl delete pvc ghidra-pvc-${1}
